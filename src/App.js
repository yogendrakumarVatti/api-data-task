import React,{Component} from 'react';
import './App.css';
import Post from './components/Post';


class App extends Component {
  constructor()
  {
    super();
    this.state={
      data:[],
      gets:false
    }
  }

  handleChange=()=>{
    console.log("got it");
    this.setState({
      gets:true
    })
  }

  componentDidMount()
  {
      fetch('https://reqres.in/api/users?page=2')
      .then((response)=>response.json())
      .then((finalresponse)=>
      {
        console.log(finalresponse.data);
         this.setState({
           data:finalresponse.data
         })
      })
  }
 

  render(){
  return (
     <div>
       {!this.state.gets ?
       <div>
         <div id="title" className="text-center">
           <h1>Regres Data(Get Request)</h1>
         </div>
           {
             this.state.data.map((dynamicData) =>
             <div className="text-center mt-5" key={dynamicData.id}>
              <span>Id: {dynamicData.id}</span>,
              <span> email: {dynamicData.email}</span>,
              <span> first_name: {dynamicData.first_name}</span>,
              <span> last_name: {dynamicData.last_name}</span>,
              <span> avatar: {dynamicData.avatar}</span>
         </div>)
            }
         </div>: null}

        {!this.state.gets ?
        <div className="text-center">
          <button to="/" className="btn btn-primary mt-5" onClick={this.handleChange}>Add User
          </button>
        </div>:null}

        {this.state.gets ?
        <div>
          <Post/>
        </div>: null}
    </div>
  );
}
}

export default App;



