import React from "react";
import { MDBCol, MDBInput, MDBBtn } from "mdbreact";
import axios from "axios";

class FormsPage extends React.Component {
  state = {
    fname: "",
    lname: "",
    email: ""
  };

  changeHandler = event => {
    this.setState({ 
      button:false,
      [event.target.name]: event.target.value
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    const { fname,lname,email } = this.state;
    alert('your details are successfully submitted');
    axios.post('https://reqres.in/api/users',{ fname,lname,email})
    .then((result) => {
      console.log(result);
      console.log(result.data.fname,result.data.lname,result.data.email);
    });
  }

  render() {
    console.log(this.state.button);
    const {fname,lname,email} = this.state;
    return (
     <div>
        <div>
          <h1 className="text-center">Input Data</h1>
          <div className="justify-content-center col-lg-12">
            <form onSubmit={this.onSubmit} className="form-control">
             <MDBCol md="8">
              <MDBInput
                value={fname}
                name="fname"
                onChange={this.changeHandler}
                type="text"
                label="First name"
                required
              />
             </MDBCol>
             <MDBCol  md="8">
              <MDBInput
                value={lname}
                name="lname"
                onChange={this.changeHandler}
                type="text"
                label="Last name"
                required
              />
             </MDBCol>
             <MDBCol md="8">
              <MDBInput
                value={email}
                onChange={this.changeHandler}
                type="email"
                name="email"
                label="Your Email address"
                required
              />
             </MDBCol>
             {!this.state.button ?
             <MDBBtn color="success" type="submit" onClick={this.changeHandler}>
               Submit Form
             </MDBBtn>:null}
            </form>
            {/* <div>
            <span>First_name: {this.state.fname}</span><br></br>
            <span>Last_name: {this.state.lname}</span><br></br>
            <span>Email: {this.state.email}</span>
            </div> */}
          </div>
        </div>
      </div>
    );
  }
}

export default FormsPage;